#include <rclcpp/rclcpp.hpp>
#include <tf2/transform_datatypes.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/transform_broadcaster.h>
#include "tf2_ros/static_transform_broadcaster.h"

#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.hpp>
#include <pcl/filters/voxel_grid.h>

#include "sensor_msgs/msg/point_cloud2.hpp"
#include "geometry_msgs/msg/transform_stamped.hpp"


namespace rbs_perception
{
    class PCFilter : public rclcpp::Node
    {
        public:
            explicit PCFilter();
        private:
            // Publish
            void pub_callback(rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr publisher,
                               pcl::PointCloud<pcl::PointXYZ> point_cloud);
            void make_transforms(std::string child);
            rclcpp::TimerBase::SharedPtr timer_;
            rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr publisher_;
            size_t count_;
            // Sub
            void sub_callback(const sensor_msgs::msg::PointCloud2 msg);
            rclcpp::Subscription<sensor_msgs::msg::PointCloud2>::SharedPtr subscriber_;
            geometry_msgs::msg::TransformStamped standform;
            pcl::VoxelGrid<pcl::PointXYZ> voxel_filter;
            std::unique_ptr<tf2_ros::Buffer> tf_buffer_;
            std::shared_ptr<tf2_ros::TransformListener> tf_listener_{nullptr};
            std::unique_ptr<tf2_ros::TransformBroadcaster> br;
            std::string world_frame = "world";
            std::shared_ptr<tf2_ros::StaticTransformBroadcaster> tf_static_broadcaster_;
    };
}

int main(int argc, char *argv[])
{
    /*
     * INITIALIZE ROS NODE
     */
    rclcpp::init(argc, argv);
    auto node = std::make_shared<rbs_perception::PCFilter>();
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}

