FROM nvidia/cuda:12.6.2-cudnn-devel-ubuntu22.04
ARG WSDIR=rbs_ws

ENV RBS_ASSEMBLY_DIR=/assembly
ENV DEBIAN_FRONTEND=noninteractive

SHELL ["/bin/bash", "-c"]

# COPY /home/bill-finger/assembly /assembly
ENV IGN_GAZEBO_RESOURCE_PATH=/${WSDIR}/install/rbs_simulation/share/rbs_simulation/

RUN apt-get update && apt-get upgrade -y && apt-get install -y \ 
    git \
    software-properties-common \
    python3-pip \ 
    lsb-release \
    curl \
    wget \
    libgeos-dev

RUN add-apt-repository universe
RUN curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(. /etc/os-release && echo $UBUNTU_CODENAME) main" | tee /etc/apt/sources.list.d/ros2.list > /dev/null


# Insatll ROS2 and tools
RUN apt-get update &&\
  apt-get upgrade &&\
  apt-get install -y ros-humble-ros-base\
  python3-rosdep\
  python3-colcon-common-extensions

WORKDIR /${RBS_ASSEMBLY_DIR}
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
RUN apt-get install git-lfs

WORKDIR /${WSDIR}

COPY . src/robossembler-ros2/

RUN pip install vcstool

# Install framework and dependencies
RUN vcs import src/. < src/robossembler-ros2/repos/all-deps.repos
RUN pip install -r src/robossembler-ros2/repos/requirements.txt
RUN apt-get update && source /opt/ros/humble/setup.bash && rosdep init && rosdep update && \
    rosdep install -y -r -q --from-paths src --ignore-src --rosdistro humble
RUN . /opt/ros/humble/setup.bash && \
    colcon build --symlink-install --cmake-args -DCMAKE_EXPORT_COMPILE_COMMANDS=1



WORKDIR /${WSDIR}
