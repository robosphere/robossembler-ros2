#include "Eigen/Dense"
#include "control_msgs/action/follow_joint_trajectory.hpp"
#include "kdl/chainiksolverpos_lma.hpp"
#include "rbs_skill_servers/base_skill.hpp"
#include <cstddef>
#include <iterator>
#include <kdl/chain.hpp>
#include <kdl/chainiksolver.hpp>
#include <kdl/chainiksolvervel_wdls.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/frames.hpp>
#include <kdl/jntarray.hpp>
#include <kdl/tree.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <memory>
#include <rbs_skill_interfaces/action/detail/moveit_send_pose__struct.hpp>
#include <rclcpp/logging.hpp>
#include <rclcpp/parameter_client.hpp>
#include <rclcpp_action/client.hpp>
#include <rclcpp_action/client_goal_handle.hpp>
#include <rclcpp_action/create_client.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <string>
#include <tf2_eigen/tf2_eigen.hpp>
#include <tf2_geometry_msgs/tf2_geometry_msgs.hpp>
#include <vector>

namespace rbs_skill_actions {

using MoveitSendPose = rbs_skill_interfaces::action::MoveitSendPose;
using GoalHandleMoveitSendJointStates =
    rclcpp_action::ServerGoalHandle<MoveitSendPose>;
using FollowJointTrajectory = control_msgs::action::FollowJointTrajectory;
using FollowJointTrajectoryGoalHandle =
    rclcpp_action::ClientGoalHandle<FollowJointTrajectory>;

class MoveToPose : public SkillBase<MoveitSendPose> {
public:
  explicit MoveToPose(
      const rclcpp::NodeOptions &options = rclcpp::NodeOptions())
      : SkillBase<MoveitSendPose>("mtp_jtc", options) {

    m_joint_trajectory_client =
        rclcpp_action::create_client<FollowJointTrajectory>(
            this, "/joint_trajectory_controller/follow_joint_trajectory");

    auto cbg =
        this->create_callback_group(rclcpp::CallbackGroupType::Reentrant);

    rclcpp::SubscriptionOptions s_options;
    s_options.callback_group = cbg;

    m_joint_state_subscriber =
        this->create_subscription<sensor_msgs::msg::JointState>(
            "/joint_states", 10,
            std::bind(&MoveToPose::jointStateCallback, this,
                      std::placeholders::_1),
            s_options);
    this->declare_parameter("robot_description", "");
    this->declare_parameter("base_link", "");
    this->declare_parameter("ee_link", "");

    m_robot_description = this->get_parameter("robot_description").as_string();
    m_base_link = this->get_parameter("base_link").as_string();
    m_ee_link = this->get_parameter("ee_link").as_string();
  }

protected:
  std::string requiredActionController() override {
    return "joint_trajectory_controller";
  }

  std::vector<std::string> requiredParameters() override { return {"joints"}; }

  void executeAction() override {
    RCLCPP_INFO(this->get_logger(), "Starting executing action goal");

    auto trajectory_goal = FollowJointTrajectory::Goal();
    auto joints = this->get_parameters(requiredParameters());
    m_joint_names = joints.at(0).as_string_array();

    trajectory_goal.trajectory.joint_names = m_joint_names;

    const int max_wait_iterations = 100;
    int wait_count = 0;
    while (m_current_joint_positions.empty() &&
           wait_count++ < max_wait_iterations) {
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    if (m_current_joint_positions.empty()) {
      RCLCPP_ERROR(this->get_logger(),
                   "Joint positions were not received in time");
      return;
    }

    std::vector<double> target_joint_values;
    solveIK(target_joint_values);

    trajectory_goal.trajectory.points =
        generate_trajectory(m_current_joint_positions, target_joint_values,
                            m_current_goal->duration);

    // RCLCPP_INFO(this->get_logger(), "Starting executing action goal");
    auto send_goal_options =
        rclcpp_action::Client<FollowJointTrajectory>::SendGoalOptions();
    send_goal_options.result_callback =
        [this](const FollowJointTrajectoryGoalHandle::WrappedResult
                   &wrapped_result) {
          if (wrapped_result.code == rclcpp_action::ResultCode::SUCCEEDED) {
            RCLCPP_INFO(this->get_logger(), "Goal succeeded");
            m_current_result->success = true;
            m_current_goal_handle->succeed(m_current_result);
          } else {
            RCLCPP_ERROR(this->get_logger(), "Goal failed");
            m_current_goal_handle->abort(m_current_result);
          }
        };

    m_joint_trajectory_client->async_send_goal(trajectory_goal,
                                               send_goal_options);
  }

private:
  void jointStateCallback(const sensor_msgs::msg::JointState::SharedPtr msg) {
    if (m_joint_names.empty()) {
      return;
    }
    // RCLCPP_WARN(this->get_logger(), "Called joint positions");

    if (m_joint_mame_to_index.empty()) {
      m_joint_mame_to_index.reserve(m_joint_names.size());
      for (size_t j = 0; j < m_joint_names.size(); ++j) {
        auto it =
            std::find(msg->name.begin(), msg->name.end(), m_joint_names[j]);
        if (it != msg->name.end()) {
          size_t index = std::distance(msg->name.begin(), it);
          m_joint_mame_to_index[m_joint_names[j]] = index;
        }
      }
    }

    if (m_current_joint_positions.size() != m_joint_names.size()) {
      m_current_joint_positions.resize(m_joint_names.size(), 0.0);
    }

    for (size_t j = 0; j < m_joint_names.size(); ++j) {
      auto index_it = m_joint_mame_to_index.find(m_joint_names[j]);
      if (index_it != m_joint_mame_to_index.end()) {
        m_current_joint_positions[j] = msg->position[index_it->second];
      }
    }
  }

  std::vector<trajectory_msgs::msg::JointTrajectoryPoint>
  generate_trajectory(const std::vector<double> &start_joint_values,
                      const std::vector<double> &target_joint_values,
                      const double duration) {

    RCLCPP_INFO(this->get_logger(), "Starting generate_trajectory");
    const int num_points = 100;
    std::vector<trajectory_msgs::msg::JointTrajectoryPoint> points;
    for (int i = 0; i <= num_points; ++i) {
      trajectory_msgs::msg::JointTrajectoryPoint point;

      double t = static_cast<double>(i) / num_points;
      for (size_t j = 0; j < target_joint_values.size(); ++j) {
        double position = start_joint_values[j] +
                          t * (target_joint_values[j] - start_joint_values[j]);
        point.positions.push_back(position);
      }
      point.time_from_start = rclcpp::Duration::from_seconds(t * duration);
      points.push_back(point);
    }
    return points;
  }

  void solveIK(std::vector<double> &out) {

    RCLCPP_INFO(this->get_logger(), "Starting solveIK");
    KDL::JntArray q_in(m_joint_names.size());
    for (size_t i = 0; i < m_joint_names.size(); ++i) {
      q_in(i) = m_current_joint_positions[i];
    }

    KDL::JntArray q_out(m_joint_names.size());

    Eigen::Affine3d target_pose;
    Eigen::fromMsg(m_current_goal->target_pose, target_pose);

    KDL::Frame target_pose_kdl(
        KDL::Rotation(target_pose(0, 0), target_pose(0, 1), target_pose(0, 2),
                      target_pose(1, 0), target_pose(1, 1), target_pose(1, 2),
                      target_pose(2, 0), target_pose(2, 1), target_pose(2, 2)),
        KDL::Vector(target_pose.translation().x(),
                    target_pose.translation().y(),
                    target_pose.translation().z()));

    KDL::Tree kdl_tree;
    if (!kdl_parser::treeFromString(m_robot_description, kdl_tree)) {
      RCLCPP_ERROR(this->get_logger(),
                   "Failed to parse KDL tree from robot description.");
      return;
    }

    KDL::Chain kdl_chain;
    if (!kdl_tree.getChain(m_base_link, m_ee_link, kdl_chain)) {
      RCLCPP_ERROR(this->get_logger(),
                   "Failed to get KDL chain from base to end-effector.");
      return;
    }

    auto ik_solver =
        std::make_unique<KDL::ChainIkSolverPos_LMA>(kdl_chain, 1e-5, 500);

    if (ik_solver->CartToJnt(q_in, target_pose_kdl, q_out) >= 0) {
      out.resize(q_out.rows());
      for (size_t i = 0; i < out.size(); i++) {
        out[i] = q_out(i);
      }
    } else {
      RCLCPP_ERROR(this->get_logger(), "IK solution not found.");
    }
  }

  rclcpp_action::Client<FollowJointTrajectory>::SharedPtr
      m_joint_trajectory_client;
  rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr
      m_joint_state_subscriber;

  std::vector<double> m_current_joint_positions;
  std::unordered_map<std::string, size_t> m_joint_mame_to_index;
  std::vector<std::string> m_joint_names;
  std::string m_base_link;
  std::string m_ee_link;
  std::string m_robot_description;
};

} // namespace rbs_skill_actions

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(rbs_skill_actions::MoveToPose);
