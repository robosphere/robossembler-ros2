import rclpy
from rclpy.node import Node
import argparse

from geometry_msgs.msg import PoseStamped

class CartesianControllerPublisher(Node):

    def __init__(self, robot_name: str):
        super().__init__("cartesian_controller_pose_publisher")
        self.publisher_ = self.create_publisher(
            PoseStamped, 
            "/cartesian_motion_controller/target_frame", 10)
        timer_period = 0.5  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)

    def timer_callback(self):
        msg = PoseStamped()
        msg.header.stamp = self.get_clock().now().to_msg()
        msg.header.frame_id = "base_link"
        msg.pose.position.x = 0.2
        msg.pose.position.y = 0.2
        msg.pose.position.z = 0.2
        msg.pose.orientation.x = 0.0
        msg.pose.orientation.y = 1.0
        msg.pose.orientation.z = 0.0
        msg.pose.orientation.w = 0.0

        self.publisher_.publish(msg)


def main(args=None):
    rclpy.init(args=args)

    parser = argparse.ArgumentParser(description='ROS2 Minimal Publisher')
    parser.add_argument('--robot-name', type=str, default='arm0', help='Specify the robot name')
    args = parser.parse_args()
    minimal_publisher = CartesianControllerPublisher(args.robot_name)

    rclpy.spin(minimal_publisher)

    minimal_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
