"""
  json_description_example
  Skill description for Robossembler framework

  09.12.2024 @shalenikol release 0.1 
"""
import json

# Описание JSON-файла
json_description = {
    "skills": [
        {
            "sid": "4147d1c3-54d8-40c2-968a-ddcd41f5d882",  # Уникальный идентификатор навыка
            "SkillPackage": {
                "name": "Robossembler",  # Название пакета навыков
                "version": "1",          # Версия пакета
                "format": "1.0"          # Версия формата пакета
            },
            "Module": {
                "node_name": "lc_yolo",  # Имя узла модуля
                "name": "ObjectDetection", # Название модуля
                "description": "Object detection skill with YOLOv8"  # Описание модуля
            },
            "Launch": {  # Параметры запуска
                "executable": "od_yolo_lc.py",    # Исполняемый файл
                "package": "rbss_objectdetection" # Пакет, к которому принадлежит исполняемый файл
            },
            "BTAction": [  # Список действий дерева поведения
                {
                    "name": "odConfigure",  # Название действия
                    "type": "run",          # Тип действия
                    "param": [              # Параметры действия
                        {
                            "type": "topic",  # Тип параметра
                            "dependency": {   # Зависимость параметра
                                "type": "topic",
                                "sid": "36229f1f-24a6-4b21-8afd-1a9bf9f8abc9",  # Уникальный идентификатор зависимости
                                "topicType": "sensor_msgs/msg/Image",           # Тип топика
                                "topicOut": "/rgbd_camera/image"                # Выходной топик
                            }
                        }
                    ],
                    "typeAction": "ACTION"  # Тип узла в дереве поведения
                }
            ],
            "topicsOut": [  # Список выходных топиков
                {
                    "name": "lc_yolo/object_detection",          # Название выходного топика
                    "type": "rbs_skill_interfaces/msg/BoundBox"  # Тип сообщения выходного топика
                }
            ]
        }
    ]
}

# Пример использования
json_string = json.dumps(json_description, indent=4)
print(json_string)
