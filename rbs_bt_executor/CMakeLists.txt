cmake_minimum_required(VERSION 3.8)
project(rbs_bt_executor)

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

find_package(ament_cmake REQUIRED)
find_package(ament_cmake_python REQUIRED)
find_package(rclpy REQUIRED)
find_package(rclcpp REQUIRED)
find_package(rclcpp_action REQUIRED)
find_package(moveit_msgs REQUIRED)
find_package(moveit_core REQUIRED)
find_package(moveit_ros_planning REQUIRED)
find_package(moveit_ros_planning_interface REQUIRED)
find_package(ament_index_cpp REQUIRED)
# find_package(behavior_tree REQUIRED)
find_package(behaviortree_ros2 REQUIRED)
find_package(behaviortree_cpp REQUIRED)
find_package(geometry_msgs REQUIRED)
find_package(control_msgs REQUIRED)
find_package(lifecycle_msgs REQUIRED)
find_package(rcl_interfaces REQUIRED)
find_package(rbs_skill_interfaces REQUIRED)
find_package(env_manager_interfaces REQUIRED)
find_package(rbs_utils REQUIRED)
find_package(tf2_eigen REQUIRED)
find_package(tf2_ros REQUIRED)
find_package(rbs_utils_interfaces REQUIRED)

if (NOT CMAKE_CXX_STANDARD)
    set(CMAKE_CXX_STANDARD 17)
endif()

set(dependencies
    rclcpp
    rclcpp_action
    geometry_msgs    
    moveit_msgs
    moveit_core
    moveit_ros_planning
    moveit_ros_planning_interface
    ament_index_cpp
    rbs_skill_interfaces
    # behavior_tree
    behaviortree_ros2
    btcpp_ros2_interfaces
    control_msgs
    lifecycle_msgs
    rcl_interfaces
    env_manager_interfaces
    rbs_utils
    tf2_ros
    tf2_eigen
    rbs_utils_interfaces
)

include_directories(include)

add_library(rbs_skill_move_topose_bt_action_client SHARED src/MoveToPose.cpp)
list(APPEND plugin_libs rbs_skill_move_topose_bt_action_client)

add_library(rbs_skill_gripper_move_bt_action_client SHARED src/GripperCommand.cpp)
list(APPEND plugin_libs rbs_skill_gripper_move_bt_action_client)

add_library(rbs_get_grasp_pose_skill_client SHARED src/GetGraspPose.cpp)
list(APPEND plugin_libs rbs_get_grasp_pose_skill_client)


add_library(rbs_get_place_pose_skill_client SHARED src/GetPlacePose.cpp)
list(APPEND plugin_libs rbs_get_place_pose_skill_client)

add_library(rbs_skill_move_joint_state SHARED src/MoveToJointStates.cpp)
list(APPEND plugin_libs rbs_skill_move_joint_state)

# add_library(rbs_add_planning_scene_object SHARED src/AddPlanningSceneObject.cpp)
# list(APPEND plugin_libs rbs_add_planning_scene_object)

# add_library(rbs_assemble_process_state SHARED src/AssembleProcessState.cpp)
# list(APPEND plugin_libs rbs_assemble_process_state)

# add_library(rbs_pose_estimation SHARED src/PoseEstimation.cpp)
# list(APPEND plugin_libs rbs_pose_estimation)

add_library(rbs_object_detection SHARED src/ObjectDetection.cpp)
list(APPEND plugin_libs rbs_object_detection)

# add_library(rbs_env_manager_starter SHARED src/EnvManager.cpp)
# list(APPEND plugin_libs rbs_env_manager_starter)

add_library(rbs_skill_move_topose_array_bt_action_client SHARED src/MoveToPoseArray.cpp)
list(APPEND plugin_libs rbs_skill_move_topose_array_bt_action_client)

add_library(rbs_get_workspace SHARED src/GetWorkspace.cpp)
list(APPEND plugin_libs rbs_get_workspace)

add_library(rbs_act SHARED src/rbsBTAction.cpp)
list(APPEND plugin_libs rbs_act)

add_executable(rbs_bt_executor src/TreeRunner.cpp)
ament_target_dependencies(rbs_bt_executor ${dependencies})

add_executable(bt_exec src/BTExec.cpp)
ament_target_dependencies(bt_exec ${dependencies})

foreach(bt_plugin ${plugin_libs})
    ament_target_dependencies(${bt_plugin} ${dependencies})
    target_compile_definitions(${bt_plugin} PRIVATE BT_PLUGIN_EXPORT)
endforeach()

install(DIRECTORY launch bt_trees config DESTINATION share/${PROJECT_NAME})

# INSTALL PLUGIN TARGETS https://github.com/BehaviorTree/BehaviorTree.CPP/pull/804
install(TARGETS
    ${plugin_libs}
    ARCHIVE DESTINATION share/${PROJECT_NAME}/bt_plugins
    LIBRARY DESTINATION share/${PROJECT_NAME}/bt_plugins
    RUNTIME DESTINATION share/${PROJECT_NAME}/bt_plugins
)

install(TARGETS
    rbs_bt_executor bt_exec
    ARCHIVE DESTINATION lib
    LIBRARY DESTINATION lib
    RUNTIME DESTINATION lib/${PROJECT_NAME}
)

# Install Python modules
ament_python_install_package(${PROJECT_NAME})

# Install Python executables
install(PROGRAMS
  scripts/rbs_interface.py
  scripts/bt_param.py
  DESTINATION lib/${PROJECT_NAME}
)

if(BUILD_TESTING)
  find_package(ament_lint_auto REQUIRED)
  # the following line skips the linter which checks for copyrights
  # comment the line when a copyright and license is added to all source files
  set(ament_cmake_copyright_FOUND TRUE)
  # the following line skips cpplint (only works in a git repo)
  # comment the line when this package is in a git repo and when
  # a copyright and license is added to all source files
  set(ament_cmake_cpplint_FOUND TRUE)
  ament_lint_auto_find_test_dependencies()
endif()

ament_export_dependencies(${dependencies})

ament_package()
