#include "behaviortree_ros2/bt_service_node.hpp"

#include "behaviortree_ros2/plugins.hpp"
#include <behaviortree_cpp/basic_types.h>
#include <geometry_msgs/msg/detail/point__struct.hpp>
#include <geometry_msgs/msg/detail/pose__struct.hpp>
#include <geometry_msgs/msg/detail/pose_array__struct.hpp>
#include <geometry_msgs/msg/detail/quaternion__struct.hpp>
#include <memory>
#include <rbs_utils_interfaces/srv/detail/get_grasp_pose__struct.hpp>
#include <rclcpp/logging.hpp>
#include <string>

using GetGraspPoseService = rbs_utils_interfaces::srv::GetGraspPose;
using namespace BT;

class GetGraspPose : public RosServiceNode<GetGraspPoseService> {
public:
  GetGraspPose(const std::string &name, const NodeConfig &conf,
               const RosNodeParams &params)
      : RosServiceNode<GetGraspPoseService>(name, conf, params) {

    RCLCPP_INFO(this->logger(), "Starting GetGraspPose");
  }

  static PortsList providedPorts() {
    return providedBasicPorts(
        {InputPort<std::string>("object_name"),
         OutputPort<std::shared_ptr<geometry_msgs::msg::Pose>>("grasp_pose"),
         OutputPort<std::shared_ptr<geometry_msgs::msg::Pose>>("pregrasp_pose"),
         OutputPort<std::shared_ptr<geometry_msgs::msg::Pose>>(
             "postgrasp_pose")});
  }

  bool setRequest(Request::SharedPtr &request) override {
    RCLCPP_INFO(this->logger(), "Starting send request");
    if (!getInput("object_name", request->model_name)) {
      RCLCPP_ERROR(this->node_.lock()->get_logger(),
                   "Failed to get object_name from input port");
      return false;
    }
    return true;
  }

  NodeStatus onResponseReceived(const Response::SharedPtr &response) override {
    if (!response->ok) {
      RCLCPP_ERROR(this->node_.lock()->get_logger(),
                   "Response indicates failure.");
      return NodeStatus::FAILURE;
    }

    RCLCPP_INFO(this->node_.lock()->get_logger(),
                "Response received successfully.");

    auto logPose = [this](const std::string &pose_name,
                          const geometry_msgs::msg::Pose &pose) {
      RCLCPP_INFO(this->node_.lock()->get_logger(),
                  "%s:\n"
                  "  Position: x = %.4f, y = %.4f, z = %.4f\n"
                  "  Orientation: x = %.4f, y = %.4f, z = %.4f, w = %.4f",
                  pose_name.c_str(), pose.position.x, pose.position.y,
                  pose.position.z, pose.orientation.x, pose.orientation.y,
                  pose.orientation.z, pose.orientation.w);
    };

    logPose("Grasp Pose", response->grasp_pose);
    logPose("Pre-grasp Pose", response->pregrasp_pose);
    logPose("Post-grasp Pose", response->postgrasp_pose);

    auto grasp_pose = std::make_shared<geometry_msgs::msg::Pose>();
    auto pregrasp_pose = std::make_shared<geometry_msgs::msg::Pose>();
    auto postgrasp_pose = std::make_shared<geometry_msgs::msg::Pose>();

    *grasp_pose = response->grasp_pose;
    *pregrasp_pose = response->pregrasp_pose;
    *postgrasp_pose = response->postgrasp_pose;

    setOutput("grasp_pose", grasp_pose);
    setOutput("pregrasp_pose", pregrasp_pose);
    setOutput("postgrasp_pose", postgrasp_pose);

    return NodeStatus::SUCCESS;
  }
  // virtual NodeStatus onFailure(ServiceNodeErrorCode error) override {}
};

CreateRosNodePlugin(GetGraspPose, "GetGraspPose");
