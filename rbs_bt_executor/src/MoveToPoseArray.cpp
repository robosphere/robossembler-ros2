#include "behaviortree_ros2/bt_action_node.hpp"
#include "geometry_msgs/msg/pose_array.hpp"
#include "rbs_skill_interfaces/action/moveit_send_pose.hpp"
#include <behaviortree_cpp/tree_node.h>
#include <behaviortree_ros2/plugins.hpp>
#include <behaviortree_ros2/ros_node_params.hpp>
#include <geometry_msgs/msg/detail/pose_array__struct.hpp>

using namespace BT;
using MoveToPoseArrayAction = rbs_skill_interfaces::action::MoveitSendPose;

class MoveToPoseArray : public RosActionNode<MoveToPoseArrayAction> {
public:
  MoveToPoseArray(const std::string &name, const NodeConfig &conf,
                  const RosNodeParams &params)
      : RosActionNode<MoveToPoseArrayAction>(name, conf, params) {}

  static PortsList providedPorts() {
    return providedBasicPorts(
        {InputPort<std::string>("robot_name"),
         InputPort<std::shared_ptr<geometry_msgs::msg::PoseArray>>(
             "pose_vec_in"),
         OutputPort<std::shared_ptr<geometry_msgs::msg::PoseArray>>(
             "pose_vec_out")});
  }

  bool setGoal(RosActionNode::Goal &goal) override {
    getInput("robot_name", goal.robot_name);
    getInput("pose_vec_in", m_pose_vec);

    if (m_pose_vec->poses.empty()) {
      return false;
    }
    goal.target_pose = m_pose_vec->poses[0];
    goal.end_effector_velocity = 0.3;
    goal.end_effector_acceleration = 0.3;
    goal.duration = 2.0;
    m_pose_vec->poses.erase(m_pose_vec->poses.begin());

    setOutput("pose_vec_out", m_pose_vec);

    return true;
  }

  NodeStatus onResultReceived(const WrappedResult &wr) override {
    if (!wr.result->success) {
      return NodeStatus::FAILURE;
    }
    return NodeStatus::SUCCESS;
  }

private:
  std::shared_ptr<geometry_msgs::msg::PoseArray> m_pose_vec;
};

CreateRosNodePlugin(MoveToPoseArray, "MoveToPoseArray");
