#include "behaviortree_ros2/bt_service_node.hpp"

#include "rbs_utils_interfaces/srv/get_workspace.hpp"
#include <behaviortree_cpp/basic_types.h>
#include "behaviortree_ros2/plugins.hpp"
#include <geometry_msgs/msg/detail/point__struct.hpp>
#include <geometry_msgs/msg/detail/pose_array__struct.hpp>
#include <geometry_msgs/msg/detail/quaternion__struct.hpp>
#include <string>

using GetWorkspaceService = rbs_utils_interfaces::srv::GetWorkspace;
using namespace BT;

class GetWorkspace : public RosServiceNode<GetWorkspaceService> {
public:
  GetWorkspace(const std::string &name, const NodeConfig &conf,
                    const RosNodeParams &params)
      : RosServiceNode<GetWorkspaceService>(name, conf, params) {}

  static PortsList providedPorts() {
    return providedBasicPorts({
        InputPort<std::string>("type"),
        OutputPort<std::shared_ptr<geometry_msgs::msg::PoseArray>>("workspace")
    });
  }

  bool setRequest(Request::SharedPtr &request) override {
    getInput("type", request->type);
    return true;
  }
  NodeStatus onResponseReceived(const Response::SharedPtr &response) override {
      if (!response->ok) {
          return NodeStatus::FAILURE;
      }

      auto workspace = response->workspace;
      auto workspace_arr = std::make_shared<geometry_msgs::msg::PoseArray>();
      auto quat = std::make_shared<geometry_msgs::msg::Quaternion>();

      quat->w = 0.0;
      quat->x = 0.0;
      quat->y = 1.0;
      quat->z = 0.0;

      workspace_arr->poses.resize(workspace.size());

      size_t i = 0;
      for (auto& point : workspace) {
          point.z += 0.35;

          geometry_msgs::msg::Pose pose;
          pose.position = point;
          pose.orientation = *quat;

          workspace_arr->poses[i++] = pose;
      }

      setOutput("workspace", workspace_arr);
      return NodeStatus::SUCCESS;
  }
  // virtual NodeStatus onFailure(ServiceNodeErrorCode error) override {}
};

CreateRosNodePlugin(GetWorkspace, "GetWorkspace");

