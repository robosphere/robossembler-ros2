#include "behaviortree_ros2/bt_service_node.hpp"

#include "behaviortree_ros2/plugins.hpp"
#include <behaviortree_cpp/basic_types.h>
#include <geometry_msgs/msg/detail/point__struct.hpp>
#include <geometry_msgs/msg/detail/pose__struct.hpp>
#include <geometry_msgs/msg/detail/pose_array__struct.hpp>
#include <geometry_msgs/msg/detail/quaternion__struct.hpp>
#include <memory>
#include <rbs_utils_interfaces/srv/detail/get_place_pose__struct.hpp>
#include <rclcpp/logging.hpp>
#include <string>

using GetPlacePoseService = rbs_utils_interfaces::srv::GetPlacePose;
using namespace BT;

class GetPlacePose : public RosServiceNode<GetPlacePoseService> {
public:
  GetPlacePose(const std::string &name, const NodeConfig &conf,
               const RosNodeParams &params)
      : RosServiceNode<GetPlacePoseService>(name, conf, params) {

    RCLCPP_INFO(this->logger(), "Starting GetPlacePose");
  }

  static PortsList providedPorts() {
    return providedBasicPorts(
        {InputPort<std::string>("place_pose_name"),
         OutputPort<std::shared_ptr<geometry_msgs::msg::Pose>>("place_pose"),
         OutputPort<std::shared_ptr<geometry_msgs::msg::Pose>>("preplace_pose"),
         OutputPort<std::shared_ptr<geometry_msgs::msg::Pose>>(
             "postplace_pose")});
  }

  bool setRequest(Request::SharedPtr &request) override {
    RCLCPP_INFO(this->logger(), "Starting send request");
    if (!getInput("place_pose_name", request->place_pose_name)) {
      RCLCPP_ERROR(this->node_.lock()->get_logger(),
                   "Failed to get place_pose_name from input port");
      return false;
    }
    return true;
  }

  NodeStatus onResponseReceived(const Response::SharedPtr &response) override {
    if (!response->ok) {
      RCLCPP_ERROR(this->node_.lock()->get_logger(),
                   "Response indicates failure.");
      return NodeStatus::FAILURE;
    }

    RCLCPP_INFO(this->node_.lock()->get_logger(),
                "Response received successfully.");

    auto logPose = [this](const std::string &pose_name,
                          const geometry_msgs::msg::Pose &pose) {
      RCLCPP_INFO(this->node_.lock()->get_logger(),
                  "%s:\n"
                  "  Position: x = %.4f, y = %.4f, z = %.4f\n"
                  "  Orientation: x = %.4f, y = %.4f, z = %.4f, w = %.4f",
                  pose_name.c_str(), pose.position.x, pose.position.y,
                  pose.position.z, pose.orientation.x, pose.orientation.y,
                  pose.orientation.z, pose.orientation.w);
    };

    logPose("Place Pose", response->place_pose);
    logPose("Pre-place Pose", response->preplace_pose);
    logPose("Post-place Pose", response->postplace_pose);

    auto place_pose = std::make_shared<geometry_msgs::msg::Pose>();
    auto preplace_pose = std::make_shared<geometry_msgs::msg::Pose>();
    auto postplace_pose = std::make_shared<geometry_msgs::msg::Pose>();

    *place_pose = response->place_pose;
    *preplace_pose = response->preplace_pose;
    *postplace_pose = response->postplace_pose;

    setOutput("place_pose", place_pose);
    setOutput("preplace_pose", preplace_pose);
    setOutput("postplace_pose", postplace_pose);

    return NodeStatus::SUCCESS;
  }
  // virtual NodeStatus onFailure(ServiceNodeErrorCode error) override {}
};

CreateRosNodePlugin(GetPlacePose, "GetPlacePose");
