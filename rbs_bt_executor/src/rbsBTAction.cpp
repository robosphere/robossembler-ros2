// #include "behavior_tree/BtService.hpp"
#include "behaviortree_ros2/bt_service_node.hpp"
#include "rbs_skill_interfaces/srv/rbs_bt.hpp"
#include <behaviortree_cpp/tree_node.h>
#include <behaviortree_ros2/plugins.hpp>
#include <behaviortree_ros2/ros_node_params.hpp>

#define STR_ACTION  "do"
#define STR_SID     "sid"
#define STR_COMMAND "command"
#define NODE_NAME   "rbs_interface"

using namespace BT;
using namespace std::chrono_literals;
using RbsBtActionSrv = rbs_skill_interfaces::srv::RbsBt;

class RbsBtAction : public RosServiceNode<RbsBtActionSrv> {
public:
  RbsBtAction(const std::string &name, const NodeConfig& conf, const RosNodeParams& params)
      : RosServiceNode<RbsBtActionSrv>(name, conf, params) {

    m_action_name = getInput<std::string>(STR_ACTION).value();
    RCLCPP_INFO_STREAM(logger(), "Start node RbsBtAction: " + m_action_name);

    m_params_client = std::make_shared<rclcpp::AsyncParametersClient>(node_.lock(), NODE_NAME);

    while (!m_params_client->wait_for_service(1s)) {
      if (!rclcpp::ok()) {
        RCLCPP_ERROR(logger(), "Interrupted while waiting for the service. Exiting.");
        break;
      }
      RCLCPP_WARN(logger(), NODE_NAME " service not available, waiting again...");
    }
  }

  bool setRequest(Request::SharedPtr &request) override {
    request->action = m_action_name; 
    getInput(STR_SID, request->sid);
    getInput(STR_COMMAND, request->command);
    return true;
  }

  NodeStatus onResponseReceived(const Response::SharedPtr &response) override {
    if (!response->ok) {
      return NodeStatus::FAILURE;
    }
    return NodeStatus::SUCCESS;
  }

  static PortsList providedPorts() {
    return providedBasicPorts({
        InputPort<std::string>(STR_SID),
        InputPort<std::string>(STR_ACTION),
        InputPort<std::string>(STR_COMMAND)
    });
  }

private:
  std::string m_action_name{};
  std::shared_ptr<rclcpp::AsyncParametersClient> m_params_client;
};

// !!! теперь устаревшая версия !!!
CreateRosNodePlugin(RbsBtAction, "RbsBtAction")
