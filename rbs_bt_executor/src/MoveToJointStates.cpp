#include "behaviortree_ros2/bt_action_node.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "rbs_skill_interfaces/action/moveit_send_joint_states.hpp"
#include <behaviortree_cpp/basic_types.h>
#include <behaviortree_cpp/tree_node.h>
#include <behaviortree_ros2/plugins.hpp>
#include <behaviortree_ros2/ros_node_params.hpp>

using namespace BT;
using MoveToJointStateAction =
    rbs_skill_interfaces::action::MoveitSendJointStates;

class MoveToJointState : public RosActionNode<MoveToJointStateAction> {
public:
  MoveToJointState(const std::string &name, const NodeConfig &conf,
                   const RosNodeParams &params)
      : RosActionNode<MoveToJointStateAction>(name, conf, params) {}

  static PortsList providedPorts() {
    return providedBasicPorts({InputPort<std::string>("robot_name"),
                               InputPort<std::vector<double>>("JointState")});
  }

  bool setGoal(RosActionNode::Goal &goal) override {
    getInput("robot_name", goal.robot_name);
    getInput("JointState", goal.joint_values);
    return true;
  }

  NodeStatus onResultReceived(const WrappedResult &wr) override {

    if (!wr.result->success) {
      return NodeStatus::FAILURE;
    }
    return NodeStatus::SUCCESS;
  }
};

CreateRosNodePlugin(MoveToJointState, "MoveToJointState");
