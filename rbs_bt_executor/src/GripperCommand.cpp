#include "behaviortree_ros2/bt_action_node.hpp"
#include <behaviortree_cpp/basic_types.h>
#include <behaviortree_cpp/tree_node.h>
#include <behaviortree_ros2/plugins.hpp>
#include <behaviortree_ros2/ros_node_params.hpp>
#include <control_msgs/action/detail/gripper_command__struct.hpp>
#include <rclcpp/logging.hpp>

using GripperCommand = control_msgs::action::GripperCommand;
using namespace BT;

class GripperControl : public RosActionNode<GripperCommand> {
public:
  GripperControl(const std::string &name, const NodeConfig &conf,
                 const RosNodeParams &params)
      : RosActionNode<GripperCommand>(name, conf, params) {

  }

  static PortsList providedPorts() {
    return providedBasicPorts({
        InputPort<std::string>("state"),
        InputPort<double>("open_limit"),
        InputPort<double>("close_limit"),
    });
  }

  bool setGoal(RosActionNode::Goal &goal) override {

    auto state = getInput<std::string>("state").value();
    auto open_limit = getInput<double>("open_limit").value();
    auto close_limit = getInput<double>("close_limit").value();

    position.open = open_limit;
    position.close = close_limit;
    if (state == "open") {
      goal.command.position = position.open;
    } else if (state == "close") {
      goal.command.position = position.close;
    } else {
      RCLCPP_ERROR_STREAM(node_.lock()->get_logger(),
                          "Error: Invalid state '"
                              << state << "'. Expected 'open' or 'close'.");
      return false;
    }
    goal.command.max_effort = 3.0;
    return true;
  }

  NodeStatus onResultReceived(const WrappedResult &wr) override {
    if (!wr.result->reached_goal) {
      return NodeStatus::FAILURE;
    }
    return NodeStatus::SUCCESS;
  }

private:
  struct {
    double open = 0.008;
    double close = 0.000;
  } position;
};

CreateRosNodePlugin(GripperControl, "GripperCommand");
