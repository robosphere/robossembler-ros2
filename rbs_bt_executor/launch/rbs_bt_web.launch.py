"""
  rbs_bt_web
  ROS 2 launch program for Robossembler

  @shalenikol release 0.1
  @shalenikol release 0.2 BT v.4
  @shalenikol release 0.3 bt_path = os.path.dirname(bt_path)
"""
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import DeclareLaunchArgument, OpaqueFunction, RegisterEventHandler
from launch.substitutions import LaunchConfiguration
from launch.event_handlers import OnExecutionComplete

def launch_setup(context, *args, **kwargs):
    # Initialize Arguments
    bt_path = LaunchConfiguration("bt_path")
    bt_path = bt_path.perform(context)

    if bt_path[-4:] == ".xml":
        import os
        bt_path = os.path.dirname(bt_path)

    # rbs_bt = Node(
    #     package = "rbs_bt_executor",
    #     executable = "rbs_bt_executor",
    #     parameters = [
    #         {
    #             "plugins": ["rbs_bt_executor/bt_plugins"]
    #             # "behavior_trees": [bt_path]
    #         }
    #     ]
    # )

    bt_exec = Node(
        package="rbs_bt_executor",
        executable="bt_exec",
        arguments=[bt_path]
        # prefix=['gdbserver localhost:3000'],
    )

    bt_param = Node(
        package="rbs_bt_executor",
        executable="bt_param.py",
        parameters=[{"bt_path": bt_path}]
    )
    return [
            RegisterEventHandler(
                event_handler=OnExecutionComplete(
                    target_action=bt_param,
                    on_completion=[bt_exec],
                    # on_completion=[rbs_bt, bt_exec],
                )
            ),
            bt_param
        ] # the order of elements is strictly required

def generate_launch_description():
    declared_arguments = []
    declared_arguments.append(
        DeclareLaunchArgument(
            "bt_path",
            default_value="''",
            description="path to Behavior tree instance"
        )
    )
    return LaunchDescription(declared_arguments + [OpaqueFunction(function=launch_setup)])
