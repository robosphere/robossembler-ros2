#include "dynmsg/message_reading.hpp"
#include "dynmsg/msg_parser.hpp"
#include "dynmsg/typesupport.hpp"
#include "dynmsg/yaml_utils.hpp"
#include "geometry_msgs/msg/pose.hpp"
#include "geometry_msgs/msg/pose_array.hpp"
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_lifecycle/lifecycle_node.hpp"
#include "tf2_geometry_msgs/tf2_geometry_msgs.hpp"
#include "tf2_msgs/msg/tf_message.hpp"
#include "tf2_ros/buffer.h"
#include "tf2_ros/static_transform_broadcaster.h"
#include <Eigen/Core>
#include <fstream>
#include <geometry_msgs/msg/detail/pose__struct.hpp>
#include <geometry_msgs/msg/detail/pose_array__struct.hpp>
#include <rbs_utils_interfaces/msg/detail/assembly_config__struct.hpp>
#include <rclcpp/clock.hpp>
#include <rclcpp/logger.hpp>
#include <rclcpp/node.hpp>
#include <rclcpp/node_interfaces/node_clock_interface.hpp>
#include <tf2/convert.h>
#include <tf2_eigen/tf2_eigen.hpp>
#include <tf2_msgs/msg/detail/tf_message__struct.hpp>
#include <unordered_map>

const std::string env_dir = std::getenv("RBS_ASSEMBLY_DIR");

namespace rbs_utils {

struct EnvModel {
  std::string model_name;
  std::string mesh_path;
  std::vector<double> model_inertia;
  std::vector<double> model_pose;
  double mass;
};

class AssemblyConfigLoader {
public:
  template <typename NodePtr>
  explicit AssemblyConfigLoader(const std::string &t_assembly_dir,
                                const NodePtr &t_node)
      : AssemblyConfigLoader(t_assembly_dir,
                             t_node->get_node_logging_interface(),
                             t_node->get_node_clock_interface()) {}

  explicit AssemblyConfigLoader(
      const std::string &t_assembly_dir,
      const rclcpp::node_interfaces::NodeLoggingInterface::SharedPtr
          &t_logger_node_interface,
      const rclcpp::node_interfaces::NodeClockInterface::SharedPtr
          &t_clock_node_interface);

  tf2_msgs::msg::TFMessage getAllPossibleTfData();
  tf2_msgs::msg::TFMessage getTfData(const std::string &model_name);
  tf2_msgs::msg::TFMessage getGraspTfData(const std::string &model_name);

  std::vector<std::string> getUniqueSceneModelNames();

  void printWorkspace();
  geometry_msgs::msg::PoseArray getWorkspaceInspectorTrajectory();
  geometry_msgs::msg::Pose
  transformTrajectory(const geometry_msgs::msg::Pose &pose);
  void saveRbsConfig();
  tf2_msgs::msg::TFMessage getAdditionalPoses();

private:
  std::vector<std::string> m_env_files;
  std::vector<std::ifstream> m_env_paths;
  std::string m_assembly_dir;
  std::unique_ptr<tf2_ros::Buffer> m_tf_buffer;
  rbs_utils_interfaces::msg::AssemblyConfig m_assembly_config;
  rclcpp::Logger m_logger;
  rclcpp::Clock::SharedPtr m_clock;
  void parseRbsDb(const std::string &filepath);

  geometry_msgs::msg::PoseArray getWorkspace();
  geometry_msgs::msg::Transform
  createTransform(const geometry_msgs::msg::Pose &pose);
};

class StaticFramePublisher : public rclcpp::Node {
public:
  explicit StaticFramePublisher(const tf2_msgs::msg::TFMessage &tfs)
      : Node("rbs_static_tf") {
    m_tf_static_broadcaster =
        std::make_shared<tf2_ros::StaticTransformBroadcaster>(this);

    this->make_transforms(tfs);
  }

private:
  void make_transforms(const tf2_msgs::msg::TFMessage &tfs) {
    for (const auto &transform : tfs.transforms) {
      m_tf_static_broadcaster->sendTransform(transform);
    }
  }

  std::shared_ptr<tf2_ros::StaticTransformBroadcaster> m_tf_static_broadcaster;
};
} // namespace rbs_utils
