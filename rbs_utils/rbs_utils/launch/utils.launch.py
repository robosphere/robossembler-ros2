from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, OpaqueFunction
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


def launch_setup(context, *args, **kwargs):
    asm_config_name = LaunchConfiguration("assembly_config_name")
    namespace = LaunchConfiguration("namespace")
    use_sim_time = LaunchConfiguration("use_sim_time")

    assembly_config = Node(
        package="rbs_utils",
        executable="assembly_config_service.py",
        namespace=namespace,
        parameters=[{"use_sim_time": use_sim_time}, {"assembly_config_name": asm_config_name.perform(context)}],
        output="screen",
    )

    nodes_to_start = [
        assembly_config,
    ]
    return nodes_to_start


def generate_launch_description():
    declared_arguments = []

    declared_arguments.append(
        DeclareLaunchArgument("assembly_config_name", default_value="")
    )
    declared_arguments.append(
        DeclareLaunchArgument("use_sim_time", default_value="false")
    )
    declared_arguments.append(
        DeclareLaunchArgument("namespace", default_value="")
    )
    return LaunchDescription(
        declared_arguments + [OpaqueFunction(function=launch_setup)]
    )
