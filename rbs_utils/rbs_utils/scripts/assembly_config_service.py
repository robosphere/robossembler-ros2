#!/usr/bin/python3

import os
import rclpy
from rclpy.node import Node
import yaml
from geometry_msgs.msg import Point, Pose, Quaternion
from rbs_utils_interfaces.msg import NamedPose, RelativeNamedPose, AssemblyConfig
from rbs_utils_interfaces.srv import GetGraspPose, GetWorkspace, GetPlacePose
from rbs_assets_library import get_asm_config
from env_manager.utils import Tf2Broadcaster
from env_manager.utils import Tf2Listener
import re


class AssemblyConfigService(Node):
    def __init__(self, node_name="assembly_config"):
        super().__init__(node_name)

        # Initialize parameters
        self.declare_parameter("assembly_config_name", "board_pick_and_place")
        self.declare_parameter("base_link", "world")
        # self.declare_parameter("gripper_offset", 0.055)
        config_name = (
            self.get_parameter("assembly_config_name")
            .get_parameter_value()
            .string_value
        )
        if config_name:
            self.get_logger().info(f"Loading assembly config with name: {config_name}")
            asm_config_filepath = get_asm_config(config_name)
            yaml_file = os.path.join(asm_config_filepath)
            self.assembly_config = parse_assembly_config(yaml_file)

            tf2_broadcaster = Tf2Broadcaster(self)
            self.tf2_listner = Tf2Listener(self)
            for relative_part in self.assembly_config.relative_part:
                tf2_broadcaster.broadcast_tf(
                    relative_part.relative_at,
                    relative_part.name,
                    translation=(
                        relative_part.pose.position.x,
                        relative_part.pose.position.y,
                        relative_part.pose.position.z,
                    ),
                    rotation=(
                        relative_part.pose.orientation.x,
                        relative_part.pose.orientation.y,
                        relative_part.pose.orientation.z,
                        relative_part.pose.orientation.w,
                    ),
                )

            for grasp_poses in self.assembly_config.grasp_poses:
                tf2_broadcaster.broadcast_tf(
                    grasp_poses.relative_at,
                    grasp_poses.name,
                    translation=(
                        grasp_poses.pose.position.x,
                        grasp_poses.pose.position.y,
                        grasp_poses.pose.position.z,
                    ),
                    rotation=(
                        grasp_poses.pose.orientation.x,
                        grasp_poses.pose.orientation.y,
                        grasp_poses.pose.orientation.z,
                        grasp_poses.pose.orientation.w,
                    ),
                )

            for place_poses in self.assembly_config.place_poses:
                tf2_broadcaster.broadcast_tf(
                    place_poses.relative_at,
                    place_poses.name,
                    translation=(
                        place_poses.pose.position.x,
                        place_poses.pose.position.y,
                        place_poses.pose.position.z,
                    ),
                    rotation=(
                        place_poses.pose.orientation.x,
                        place_poses.pose.orientation.y,
                        place_poses.pose.orientation.z,
                        place_poses.pose.orientation.w,
                    ),
                )

            # Services
            self.workspace_service = self.create_service(
                GetWorkspace, "get_workspace", self.get_workspace_callback
            )
            self.grasp_poses_service = self.create_service(
                GetGraspPose, "get_grasp_poses", self.get_grasp_poses_callback
            )
            self.place_pose_service = self.create_service(
                GetPlacePose, "get_place_poses", self.get_place_poses_callback
            )

    def get_workspace_callback(self, request, response):
        self.get_logger().info("Workspace request received")
        response.workspace = self.assembly_config.workspace
        response.ok = True
        return response

    def get_place_poses_callback(self, request: GetPlacePose.Request, response):
        # Compile regex patterns for the various place poses

        place_pose_pattern = re.compile(
            rf"^{re.escape(request.place_pose_name)}_place$"
        )
        preplace_pose_pattern = re.compile(
            rf"^{re.escape(request.place_pose_name)}_preplace$"
        )
        postplace_pose_pattern = re.compile(
            rf"^{re.escape(request.place_pose_name)}_postplace$"
        )
        prepostplace_pose_pattern = re.compile(
            rf"^{re.escape(request.place_pose_name)}_prepostplace$"
        )

        # Find poses in the assembly config
        place_pose_ = next(
            (
                pose
                for pose in self.assembly_config.place_poses
                if place_pose_pattern.fullmatch(pose.name)
            ),
            None,
        )
        preplace_pose_ = next(
            (
                pose
                for pose in self.assembly_config.place_poses
                if preplace_pose_pattern.fullmatch(pose.name)
            ),
            None,
        )
        postplace_pose_ = next(
            (
                pose
                for pose in self.assembly_config.place_poses
                if postplace_pose_pattern.fullmatch(pose.name)
            ),
            None,
        )
        prepostplace_pose_ = next(
            (
                pose
                for pose in self.assembly_config.place_poses
                if prepostplace_pose_pattern.fullmatch(pose.name)
            ),
            None,
        )

        # If place_pose is not found, raise an error
        if place_pose_ is None:
            response.ok = False
            self.get_logger().error(
                f"Place pose not found for name {request.place_pose_name}"
            )
            return response

        place_pose_msg = self._create_pose_msg_from_name(place_pose_)

        # Create messages for preplace and postplace, considering prepostplace
        if prepostplace_pose_:
            preplace_pose_msg = self._create_pose_msg_from_name(prepostplace_pose_)
            postplace_pose_msg = preplace_pose_msg
        else:
            preplace_pose_msg = self._create_pose_msg_from_name(preplace_pose_)
            postplace_pose_msg = self._create_pose_msg_from_name(postplace_pose_)

        # Ensure preplace and postplace have values
        if preplace_pose_msg is None:
            preplace_pose_msg = (
                Pose()
            )  # Fill with an empty Pose object if preplace is missing

        if postplace_pose_msg is None:
            postplace_pose_msg = (
                preplace_pose_msg  # Use preplace if postplace is missing
            )

        # Fill the response
        response.place_pose = place_pose_msg
        response.preplace_pose = preplace_pose_msg
        response.postplace_pose = postplace_pose_msg
        response.ok = True
        self.get_logger().info(
            f"Place pose response prepared for {request.place_pose_name}"
        )
        return response

    def get_grasp_poses_callback(self, request, response):
        # Create regular expressions for grasp poses
        grasp_pose_pattern = re.compile(
            rf"^{re.escape(request.model_name)}_grasp_pose$"
        )
        pregrasp_pose_pattern = re.compile(
            rf"^{re.escape(request.model_name)}_pregrasp_pose$"
        )
        postgrasp_pose_pattern = re.compile(
            rf"^{re.escape(request.model_name)}_postgrasp_pose$"
        )
        prepostgrasp_pose_pattern = re.compile(
            rf"^{re.escape(request.model_name)}_prepostgrasp_pose$"
        )

        # Find poses using generators
        grasp_pose_ = next(
            (
                pose
                for pose in self.assembly_config.grasp_poses
                if grasp_pose_pattern.fullmatch(pose.name)
            ),
            None,
        )
        pregrasp_pose_ = next(
            (
                pose
                for pose in self.assembly_config.grasp_poses
                if pregrasp_pose_pattern.fullmatch(pose.name)
            ),
            None,
        )
        postgrasp_pose_ = next(
            (
                pose
                for pose in self.assembly_config.grasp_poses
                if postgrasp_pose_pattern.fullmatch(pose.name)
            ),
            None,
        )
        prepostgrasp_pose_ = next(
            (
                pose
                for pose in self.assembly_config.grasp_poses
                if prepostgrasp_pose_pattern.fullmatch(pose.name)
            ),
            None,
        )

        # If grasp_pose is not found, raise an error
        if grasp_pose_ is None:
            response.ok = False
            raise RuntimeError(f"Grasp pose not found for model {request.model_name}")

        grasp_pose_msg = self._create_pose_msg_from_name(grasp_pose_)

        # Create messages for pregrasp and postgrasp, considering prepostgrasp
        if prepostgrasp_pose_:
            pregrasp_pose_msg = self._create_pose_msg_from_name(prepostgrasp_pose_)
            postgrasp_pose_msg = pregrasp_pose_msg
        else:
            pregrasp_pose_msg = self._create_pose_msg_from_name(pregrasp_pose_)
            postgrasp_pose_msg = self._create_pose_msg_from_name(postgrasp_pose_)

        # Ensure pregrasp and postgrasp have values
        if pregrasp_pose_msg is None:
            pregrasp_pose_msg = (
                Pose()
            )  # Fill with an empty Pose object if pregrasp is missing

        if postgrasp_pose_msg is None:
            postgrasp_pose_msg = (
                pregrasp_pose_msg  # Use pregrasp if postgrasp is missing
            )

        # Fill the response
        response.grasp_pose = grasp_pose_msg
        response.pregrasp_pose = pregrasp_pose_msg
        response.postgrasp_pose = postgrasp_pose_msg
        response.ok = True
        return response

    def _create_pose_msg(self, transform):
        # Convert the transform to a Pose message
        if transform is None:
            raise RuntimeError("Pose transform not found")

        # gripper_offset = self.get_parameter("gripper_offset").get_parameter_value().double_value

        pose_msg = Pose()
        pose_msg.position.x = transform.translation.x
        pose_msg.position.y = -transform.translation.y
        pose_msg.position.z = transform.translation.z  # + gripper_offset

        pose_msg.orientation.x = transform.rotation.x
        pose_msg.orientation.y = transform.rotation.y
        pose_msg.orientation.z = transform.rotation.z
        pose_msg.orientation.w = transform.rotation.w

        return pose_msg

    def _create_pose_msg_from_name(self, pose):
        # Convert RelativeNamedPose to Pose (stub, depends on your implementation)
        if pose is None:
            return None

        # Assumes that pose contains data that can be used to form a Pose
        transform = self.tf2_listner.lookup_transform_sync(
            target_frame=pose.name,
            source_frame=self.get_parameter("base_link")
            .get_parameter_value()
            .string_value,
        )
        return self._create_pose_msg(transform)


def parse_assembly_config(yaml_file):
    with open(yaml_file, "r") as file:
        data = yaml.safe_load(file)

    assets_db = data.get("assets_db", "")

    workspace = [Point(**point) for point in data.get("workspace", [])]

    absolute_part = []
    for part in data.get("absolute_part", []):
        position = part["pose"]["position"]
        orientation = part["pose"].get(
            "orientation", {"x": 0.0, "y": 0.0, "z": 0.0, "w": 1.0}
        )
        pose = Pose(position=Point(**position), orientation=Quaternion(**orientation))
        absolute_part.append(NamedPose(name=part["name"], pose=pose))

    relative_part = []
    for part in data.get("relative_part", []):
        position = part["pose"]["position"]
        orientation = part["pose"].get(
            "orientation", {"x": 0.0, "y": 0.0, "z": 0.0, "w": 1.0}
        )
        pose = Pose(position=Point(**position), orientation=Quaternion(**orientation))
        relative_part.append(
            RelativeNamedPose(
                name=part["name"], relative_at=part["relative_at"], pose=pose
            )
        )

    grasp_pose = []
    for pose in data.get("grasp_poses", []):
        position = pose["pose"]["position"]
        orientation = pose["pose"].get(
            "orientation", {"x": 0.0, "y": 0.0, "z": 0.0, "w": 1.0}
        )
        pose_obj = Pose(
            position=Point(**position), orientation=Quaternion(**orientation)
        )
        grasp_pose.append(
            RelativeNamedPose(
                name=pose["name"], relative_at=pose["relative_at"], pose=pose_obj
            )
        )

    extra_poses = []
    for pose in data.get("extra_poses", []):
        position = pose["pose"]["position"]
        orientation = pose["pose"].get(
            "orientation", {"x": 0.0, "y": 0.0, "z": 0.0, "w": 1.0}
        )
        pose_obj = Pose(
            position=Point(**position), orientation=Quaternion(**orientation)
        )
        extra_poses.append(NamedPose(name=pose["name"], pose=pose_obj))

    place_poses = []
    for pose in data.get("place_poses", []):
        position = pose["pose"]["position"]
        orientation = pose["pose"].get(
            "orientation", {"x": 0.0, "y": 0.0, "z": 0.0, "w": 1.0}
        )
        pose_obj = Pose(
            position=Point(**position), orientation=Quaternion(**orientation)
        )
        place_poses.append(
            RelativeNamedPose(
                name=pose["name"], relative_at=pose["relative_at"], pose=pose_obj
            )
        )

    assembly_config = AssemblyConfig(
        assets_db=assets_db,
        workspace=workspace,
        absolute_part=absolute_part,
        relative_part=relative_part,
        grasp_poses=grasp_pose,
        extra_poses=extra_poses,
        place_poses=place_poses,
    )

    return assembly_config


def main():
    rclpy.init()

    executor = rclpy.executors.SingleThreadedExecutor()
    node = AssemblyConfigService()
    executor.add_node(node)
    try:
        executor.spin()
    except (KeyboardInterrupt, rclpy.executors.ExternalShutdownException):
        node.destroy_node()


if __name__ == "__main__":
    main()
