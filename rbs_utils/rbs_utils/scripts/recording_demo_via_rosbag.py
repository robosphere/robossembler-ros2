#!/usr/bin/env python3
"""
  recording_demo_lifecycle_node_via_RosBag
  ROS 2 program for recording a demo via RosBag

  @shalenikol release 0.3
  !!! In "Humble" writer.close() not worked !!! Waiting "Jazzy"
"""
import os
import shutil
import json
from functools import partial

import rclpy
from rclpy.lifecycle import Node
from rclpy.lifecycle import State
from rclpy.lifecycle import TransitionCallbackReturn

import rosbag2_py
from ros2topic.api import get_topic_names_and_types
from rclpy.serialization import serialize_message

from sensor_msgs.msg import Image, JointState

NODE_NAME_DEFAULT = "lc_record_demo"
PARAM_SKILL_CFG   = "lc_record_demo_cfg"

OUTPUT_PATH_DEFAULT = "rbs_bag"
WRITER_ID = "sqlite3"
TOPIC_TYPES = ["sensor_msgs/msg/JointState", "sensor_msgs/msg/Image"]
TOPIC_CLASSES = [JointState, Image]

class RecordingDemo(Node):
    """ lifecycle node """

    def __init__(self, **kwargs):
        self.mode: int = 0 # 0 - нет записи, 1 - есть запись
        self.topics = []
        self._sub = []
        self.output_path = OUTPUT_PATH_DEFAULT

        # for other nodes
        kwargs["allow_undeclared_parameters"] = True
        kwargs["automatically_declare_parameters_from_overrides"] = True
        super().__init__(NODE_NAME_DEFAULT, **kwargs)
        str_cfg = self.get_parameter(PARAM_SKILL_CFG).get_parameter_value().string_value

        self.skill_cfg = json.loads(str_cfg)
        sets = self.skill_cfg["Settings"]["output"]["params"]
        for s in sets:
            if s["name"] == "output_path":
                self.output_path = s["value"]
        
    def get_list_topics(self) -> list:
        topic_names_and_types = get_topic_names_and_types(node=self, include_hidden_topics=False)

        topic_list = []
        for (topic_name, topic_types) in topic_names_and_types:
            if topic_types[0] in TOPIC_TYPES:
                i = TOPIC_TYPES.index(topic_types[0])
                topic_list.append({"name": topic_name, "type": topic_types[0], "class": TOPIC_CLASSES[i]})

        return topic_list

    def check_output_folder(self, path: str) -> None:
        # check for output folder existence
        if os.path.isdir(path):
            x = 1
            while os.path.isdir(path + str(x)):
                x += 1
            shutil.move(path, path + str(x))

    def on_configure(self, state: State) -> TransitionCallbackReturn:
        self.check_output_folder(self.output_path)
        self.writer = rosbag2_py.SequentialWriter()

        storage_options = rosbag2_py._storage.StorageOptions(uri=self.output_path, storage_id=WRITER_ID)
        converter_options = rosbag2_py._storage.ConverterOptions("", "")
        self.writer.open(storage_options, converter_options)

        self.topics = self.get_list_topics()
        for id, topic in enumerate(self.topics):
            topic_info = rosbag2_py._storage.TopicMetadata(
                name=topic["name"],
                type=topic["type"],
                serialization_format="cdr")
            self.writer.create_topic(topic_info)
            self.get_logger().info(f"on_configure: {id}) {topic}")

        # self.get_logger().info("on_configure() is called.")
        return TransitionCallbackReturn.SUCCESS

    def on_activate(self, state: State) -> TransitionCallbackReturn:
        self.get_logger().info('on_activate() is called')
        # Create the subscribers.
        for id, topic in enumerate(self.topics):
            sub = self.create_subscription(topic["class"], topic["name"],
                                           partial(self.topic_callback, topic["name"]),
                                           10)
            self._sub.append(sub)

        self.mode += 1
        return super().on_activate(state)

    def on_deactivate(self, state: State) -> TransitionCallbackReturn:
        self.get_logger().info("on_deactivate() is called")
        # Destroy the subscribers.
        for sub in self._sub:
            self.destroy_subscription(sub)
        self._sub.clear()

        self.mode -= 1
        return super().on_deactivate(state)

    def on_cleanup(self, state: State) -> TransitionCallbackReturn:
        for sub in self._sub:
            self.destroy_subscription(sub)
        self._sub.clear()

        # self.writer.close() # / del self.writer

        self.get_logger().info("on_cleanup() is called")
        self.timer = self.create_timer(0.01, self.timer_callback)
        return TransitionCallbackReturn.SUCCESS

    def on_shutdown(self, state: State) -> TransitionCallbackReturn:
        for sub in self._sub:
            self.destroy_subscription(sub)
        self._sub.clear()

        self.get_logger().info("on_shutdown() is called")
        return TransitionCallbackReturn.SUCCESS
    
    def topic_callback(self, topic_name, msg):
        self.writer.write(
            topic_name,
            serialize_message(msg),
            self.get_clock().now().nanoseconds)

    def timer_callback(self):
        self.get_logger().info("Restarting node..")
        self.destroy_node()
        rclpy.shutdown()
        rclpy.init()
        lc_node = RecordingDemo()
        rclpy.spin(lc_node)

def main():
    rclpy.init()

    executor = rclpy.executors.SingleThreadedExecutor()
    lc_node = RecordingDemo()
    executor.add_node(lc_node)
    try:
        executor.spin()
    except (KeyboardInterrupt, rclpy.executors.ExternalShutdownException):
        lc_node.destroy_node()

if __name__ == '__main__':
    main()
