# Robossembler ROS2

Repo for ROS2 packages related to Robossembler

## Packages List

1. `env_manager` - virtual environment switching manager and related packages
    - `env_interface` - base class for creating a specific environment based on the ROS 2 LifeCycle Node 
    - `env_manager` - main package of the virtual environment switching manager
    - `env_manager_interfaces` - ROS 2 interfaces for env_manager, describe messages about the state of the environment, services for configuring/loading/enabling/unloading the environment
    - `gz_environment` - a specific instance of `env_interface` for the Gazebo simulator
    - `planning_scene_manager` - sync env with planning scene for MoveIt 2
    - `rbs_gym` - training module with reinforcement: management of the learning process, formation of simulation environments, management of action spaces (actions) and perception (observation), task management, utilities
2. `rbs_bringup` - package for launching different launch scenarios: simulation, real robot, different hardware configurations (multi-robot)
3. `rbs_bt_executor` - module for launching behavior trees on Behavior Tree CPP v4
4. `rbs_interface` - package for linking trees with skill servers
5. `rbs_perception` - machine perception module, where different versions are implemented
6. `rbs_simulation` - models for simulation
7. `rbs_skill_interfaces` - commonly used (common) interfaces for interacting with Skill Servers and the Behavior Tree (specific interfaces are placed in Skill Server packages)
8. `rbs_skill_servers` - Skill Server packages: Move, Object Detection, 6D Pose Estimation
9. `rbs_task_planner` - PDDL-based task scheduler
10. `rbs_utils` - working with a config containing capture positions for details
11. `rbss_objectdetection` - Object Detection Skill Server using YOLOv8

[Installation instructon RU](./docs/ru/installation.md)

[Installation instructon EN](./docs/en/installation.md)
