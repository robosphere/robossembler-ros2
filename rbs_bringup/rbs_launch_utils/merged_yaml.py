from typing import Dict
from typing import List
from typing import Text
from typing import Optional

import yaml
import tempfile
import launch
from nav2_common.launch import RewrittenYaml
from rbs_launch_utils.launch_common import load_yaml_abs

class MergedYaml:
    """
    Apply RewrittenYaml for list of config files and combine it in one file
    """
    def __init__(self, context: launch.LaunchContext,
                 source_file: Text = "",
                 root_keys: List = [],
                 param_rewrites: Dict = {},
                 convert_types = False,
                 key_rewrites: Dict = {}):

        self.__root_keys = root_keys
        self.__context = context
        self.__param_rewrites = param_rewrites
        self.__convet_types = convert_types
        #TODO: work with multiple source files
        # for different robots
        self.__source_file = source_file
        self.__key_rewrites = key_rewrites

    def get_config_file(self) -> List[Text]:
        config_list = []
        for key in self.__root_keys:
            yaml = RewrittenYaml(
                self.__source_file,
                self.__param_rewrites,
                key,
                self.__key_rewrites,
                self.__convet_types)
            config_list.append(yaml.perform(self.__context))
        return config_list

    def merge_yamls(self) -> Text:
        configs = self.get_config_file()
        yamls = []
        for file in configs:
            yamls.append(load_yaml_abs(file))
        merged_yaml = tempfile.NamedTemporaryFile(mode='w', delete=False, suffix=".yaml")
        yaml.dump_all(yamls, merged_yaml)
        return merged_yaml.name
