# Instructions for Adding a New Robot to the Robossembler ROS 2 Framework

First, you need to download the robot package containing `xacro` or `urdf` files, as well as geometry files in formats such as `.stl`, `.dae`, `.obj`, etc.

Before starting, it is important to understand the basics of the [xacro](https://github.com/ros/xacro/wiki) format. This format allows you to reuse existing fragments of a robot's URDF description, making it easier to create and modify descriptions.

### Steps for Adding a New Robot:

1. **Install the Robot Package**  
   After installing the robot package, create a file named `xacro_args.yaml` in the `{description_package}/config/` directory. This file should specify the arguments needed to convert the `xacro` file into a `urdf`.

2. **Configure the Launch File**  
   Edit the file [`rbs_bringup.launch.py`](../../rbs_bringup/launch/rbs_bringup.launch.py) to define the parameters required to launch the robot.

   Example of a standard implementation:
   ```python
   main_script = IncludeLaunchDescription(
       PythonLaunchDescriptionSource(
           [
               PathJoinSubstitution(
                   [FindPackageShare("rbs_runtime"), "launch", "runtime.launch.py"]
               )
           ]
       ),
       launch_arguments={
           "with_gripper": "true",
           "gripper_name": "rbs_gripper",
           "robot_type": "rbs_arm",
           "description_package": "rbs_arm",
           "description_file": "rbs_arm_modular.xacro",
           "robot_name": "rbs_arm",
           "use_moveit": "false",
           "moveit_config_package": "rbs_arm",
           "moveit_config_file": "rbs_arm.srdf.xacro",
           "use_sim_time": "true",
           "hardware": "gazebo",
           "use_controllers": "true",
           "scene_config_file": "",
           "base_link_name": "base_link",
           "ee_link_name": "gripper_grasp_point",
       }.items(),
   )
   ```
   This configuration launches another file with specific arguments. Below is a description of each argument to help you decide whether it is needed.

### Parameter Descriptions:

- **`with_gripper`**  
  Indicates whether the robot has a gripper. If set to `true`, the `gripper_controller` will be configured and launched.

- **`gripper_name`**  
  Used as a keyword to identify links and joints related to the gripper. It is also applied in `xacro` arguments.

- **`robot_type`**  
  Specifies a group of robots of the same type, allowing you to semantically group robots with different names but similar designs.

- **`description_package`**  
  The package containing the robot's URDF description. This parameter is mandatory and is used to locate the URDF file and controller configuration files.

- **`description_file`**  
  The name of the description file, which should be located in `{description_package}/urdf/`.

- **`robot_name`**  
  A unique name for the robot to distinguish it from others in the scene.

- **`use_moveit`**  
  Indicates whether [MoveIt 2](https://moveit.picknik.ai/humble/index.html) should be used. While it is not required for basic movements, it is recommended when working with obstacles.

- **`moveit_config_package`**  
  The name of the MoveIt 2 configuration package generated based on the `{description_package}`. This parameter is required if you plan to use MoveIt 2.

- **`moveit_config_file`**  
  The MoveIt 2 launch file located in `{moveit_config_package}/launch/`.

- **`use_sim_time`**  
  A mandatory parameter for simulation. It ensures that time is synchronized with the simulator.

- **`hardware`**  
  Specifies the interface to be used for controlling the robot. For example, `gazebo`. This parameter is primarily used in `xacro` files.

- **`use_controllers`**  
  Indicates whether to use standard controllers. If set to `false`, the robot will not be able to move. This parameter controls the execution of the [control.launch.py](../../rbs_bringup/launch/control.launch.py) file. You can write a custom implementation instead of using this flag.

- **`scene_config_file`**  
  A YAML file that defines the scene configuration. A default example is available [here](../../env_manager/rbs_runtime/config/default-scene-config.yaml). Ensure that the degrees of freedom of your robot match the configuration file.

- **`base_link_name`**  
  The name of the robot's base link, which defines where the robot begins. This parameter is important for skill configuration. Review your robot's URDF to set this correctly.

- **`ee_link_name`**  
  The name of the end-effector link, which defines where the robot ends. This parameter is also important for skill configuration and should be set based on the URDF.

- **`control_space`**  
  Specifies the space in which the robot will be controlled.  
  Possible values:  
  - `task` — control in task space (e.g., using positions and orientations in the workspace).  
  - `joint` — control in joint space (e.g., using angular values for each robot joint).  
  Default value: `task`.

- **`control_strategy`**  
  Specifies the control strategy for the robot.  
  Possible values:  
  - `position` — position control, where desired positions are set for joints or the workspace target point.  
  - `velocity` — velocity control, where desired movement speeds are set.  
  - `effort` — effort control, where torques or forces applied to the joints are specified.  
  Default value: `position`.

- **`interactive`**  
  Specifies whether to run the `motion_control_handle` controller.  
  Default value: `true`.  
