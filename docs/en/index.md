Robossembler ROS 2 Packages

- **`env_manager`** - virtual environment manager:
    - **`env_manager`** - manages objects in Gazebo simulation scenes.
    - **`env_manager_interfaces`** - ROS 2 interfaces for configuring, loading, activating, and unloading environments.
    - **`rbs_gym`** - reinforcement learning module: training management, simulation environment creation, action and observation space handling, utilities.
    - **`rbs_runtime`** - runs the main runtime using `env_manager`.

- **`rbs_bringup`** - launch scenarios: simulation, real robot, multi-robot configurations.
- **`rbs_bt_executor`** - executes behavior trees with Behavior Tree CPP v4.
- **`rbs_interface`** - interface linking behavior trees with skill servers (recommended to merge with `rbs_bt_executor`).
- **`rbs_perception`** - machine vision module with multiple implementations.
- **`rbs_simulation`** - simulation models (recommended to merge with `env_manager` or `rbs_gym`).
- **`rbs_skill_interfaces`** - common interfaces for interacting with skill servers and behavior trees.
- **`rbs_skill_servers`** - packages for skill servers (recommended to replace with individual packages for each server).
- **`rbs_task_planner`** - task planner based on PDDL.
- **`rbs_utils`** - utilities for working with configurations containing grasp positions.
- **`rbss_objectdetection`** - skill server for object detection using YOLOv8.
