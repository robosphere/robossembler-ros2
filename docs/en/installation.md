# Framework Installation Guide

**Important!** Compatible only with Ubuntu 22.04.  

Below are the steps to set up the framework.

---

## Step 1: Install ROS2 Humble  

Start by installing [ROS2 Humble](https://docs.ros.org/en/humble/Installation/Ubuntu-Install-Debs.html).  
A minimal installation of `ros-humble-ros-base` is recommended, along with the `ros-dev-tools` package.

---

## Step 2: Initialize `rosdep` (if not done already)  

Run the following commands:
```sh
sudo rosdep init
rosdep update
```

---

## Step 3: Verify ROS2 Environment  

Before continuing, ensure that the ROS2 environment is activated. Run:
```sh
source /opt/ros/humble/setup.bash
```

---

## Step 4: Framework Installation  

```sh
cd 
mkdir -p robossembler-ws/src && cd robossembler-ws/src
git clone --recurse-submodules https://gitlab.com/robossembler/robossembler-ros2.git
```

Next, build [`ros2_control`](https://github.com/ros-controls/ros2_control) from source using this [fork](https://github.com/solid-sinusoid/ros2_control/tree/gz-ros2-cartesian-controllers).  
Alternatively, you can use [`vsctool`](https://github.com/dirk-thomas/vcstool), which is included in the basic ROS2 packages.

If you choose to use `vcstool`, the necessary packages will be cloned into the same workspace as the framework. Use this command:
```sh
vcs import . < robossembler-ros2/repos/all-deps.repos
```

You can also install all required Python libraries by running:
```shell
pip install -r robossembler-ros2/repos/requirements.txt
# If you encounter an error while installing Shapely:
sudo apt install libgeos-dev
```

> **[!IMPORTANT]**  
> Ensure that `git lfs` is installed. The `requirements.txt` file includes the `rbs_assets_library` module, which contains large files and is installed as a Python module.

Run these commands in the `robossembler-ws/src/` directory.

Install dependencies using `rosdep`:
```sh
cd ~/robossembler-ws
rosdep install --from-paths src -y --ignore-src --rosdistro ${ROS_DISTRO}
```

Build the framework using `colcon`:
```sh
colcon build
```

---

## Full Command Sequence  

Below is the full set of commands to set up the framework:
```sh
cd 
mkdir -p robossembler-ws/src && cd robossembler-ws/src
git clone --recurse-submodules https://gitlab.com/robossembler/robossembler-ros2.git
# Or, if you prefer Radicle:
git clone --recurse-submodules https://seed.robossembler.org/z46gtVRpXaXrGQM7Fxiqu7pLy7kip.git robossembler-ros2
rad clone rad:z46gtVRpXaXrGQM7Fxiqu7pLy7kip

vcs import . < robossembler-ros2/repos/all-deps.repos
pip install -r robossembler-ros2/repos/requirements.txt
cd ..
rosdep install --from-paths src -y --ignore-src --rosdistro ${ROS_DISTRO}
colcon build
```
