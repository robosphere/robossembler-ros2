## Пример запуска симуляции (runtime) сцены с роботом

Запустить последовательно четыре терминала и выполнить запуск 3-х launch-файлов и симулятора Gazebo:  
Во всех терминалах выполнить предварительную настройку:
```sh
# ROS2
source /opt/ros/humble/setup.bash
# Robossembler
cd ~/robossembler-ws
source install/setup.bash
```

* 1 - запуск интерфейсной ноды с серверами навыков
```sh
ros2 launch rbs_bt_executor interface.launch.py bt_path:=src/robossembler-ros2/test_examples/bt_default
```
* 2 - запуск runtime с роботом в сцене
```sh
ros2 launch rbs_bringup rbs_bringup.launch.py
```
* 3 - запуск Gazebo (для визуализации процесса)
```sh
ign gazebo -g
```
* 4 - запуск дерева поведения
```sh
ros2 launch rbs_bt_executor rbs_bt_web.launch.py bt_path:=src/robossembler-ros2/test_examples/bt_default
```
Чтобы наблюдать сцену с движением робота, нужно перейти в окно Gazebo.

![gazebo_rbs_arm](gazebo_rbs_arm.png)
*визуализация сцены с роботом*

## Пример запуска симуляции в режиме `benchmark` для оценки управления роботом через разные контроллеры

Запустить последовательно четыре терминала и выполнить запуск 3-х launch-файлов и симулятора Gazebo:  
Во всех терминалах выполнить предварительную настройку:
```sh
# ROS2
source /opt/ros/humble/setup.bash
# Robossembler
cd ~/robossembler-ws
source install/setup.bash
```

* 1 - запуск интерфейсной ноды в режиме `benchmark`
```sh
ros2 launch rbs_bt_executor interface.launch.py bt_path:=src/robossembler-ros2/test_examples/bt_benchmark mode:=benchmark
```
* 2 - запуск runtime с роботом в сцене
```sh
ros2 launch rbs_bringup rbs_bringup.launch.py
```
* 3 - запуск Gazebo (для визуализации процесса)
```sh
ign gazebo -g
```
* 4 - запуск дерева поведения
```sh
ros2 launch rbs_bt_executor rbs_bt_web.launch.py bt_path:=src/robossembler-ros2/test_examples/bt_benchmark
```

В терминале 1 (интерфейсная нода) будет отражаться время выполнения каждого действия:

![benchmark_result](benchmark_result.png)
*пример вывода журнала*

## Пример запуска симуляции с записью rosbag-журнала для демонстрации в веб.

Запустить последовательно три терминала и выполнить запуск 3-х launch-файлов.  
Во всех терминалах выполнить предварительную настройку:
```sh
# ROS2
source /opt/ros/humble/setup.bash
# Robossembler
cd ~/robossembler-ws
source install/setup.bash
```

* 1 - запуск интерфейсной ноды
```sh
ros2 launch rbs_bt_executor interface.launch.py bt_path:=src/robossembler-ros2/test_examples/bt_rosbag
```
* 2 - запуск runtime с роботом в сцене
```sh
ros2 launch rbs_bringup rbs_bringup.launch.py
```
* 3 - запуск дерева поведения
```sh
ros2 launch rbs_bt_executor rbs_bt_web.launch.py bt_path:=src/robossembler-ros2/test_examples/bt_rosbag
```
 Для визуализации можно дополнительно запустить симулятор Gazebo (терминал 4):  
```sh
ign gazebo -g
```
После выполнения дерева в папке ~/robossembler-ws/rbs_testbag сохранится журнал rosbag.