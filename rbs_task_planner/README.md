### Usage
change `rbs_simulation.laucnh.py` file by providing `${PATH}` and `${PREFIX}` to assemble files
```python
    assemble_state = Node(
        package="rbs_skill_servers",
        executable="assemble_state_service_server",
        output="screen",
        emulate_tty=True,
        parameters=[
            {"assemble_prefix": "ASSEMBLE_"} # ${PREFIX}
            {"assemble_package_dir": ${COLCON_WS}/src/robossembler-ros2/rbs_task_planner/exemples/sdf_models}, # ${PATH}
        ]
    )
```
launch simulation proccess
```bash
ros2 launch rbs_simulation rbs_simulation.launch.py
```
launch task_planner process
```bash
ros2 launch rbs_task_planner task_planner.launch.py
```
start env_manager process
```bash
./${COLCON_WS}/build/env_manager/main
```
you can send `problem` throw plansys2 service with `${PROBLEM_FILE_CONTENT}`
```bash
ros2 service call /problem_expert/add_problem plansys2_msgs/srv/AddProblem '{problem: ${PROBLEM_FILE_CONTENT}}'
```
or you can use `plansys2_terminal`
```bash
ros2 run plansys2_terminal plansys2_terminal
```
after that use `plansys2_terminal` to call `run` command and start executing process
