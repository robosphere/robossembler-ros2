(define (problem Cube3-problem)
(:domain atomic_domain)
(:objects
    ur_manipulator_gripper - arm 
    ws - zone
    cube cube001 cube002 cube004 cube005 cube006 - part
    asm1 asm2 asm3 asm4 asm5 asm6 - assembly
)

(:init 
    (part_of cube asm1) 
    (part_of cube001 asm2)
    (part_of cube002 asm3)
    (part_of cube004 asm4) 
    (part_of cube005 asm5) 
    (part_of cube006 asm6) 
    (assembly_order asm1 asm2)
    (assembly_order asm2 asm3)
    (assembly_order asm3 asm4)
    (assembly_order asm4 asm5)
    (assembly_order asm5 asm6)

    (arm_available ur_manipulator_gripper)
    (assembly_at asm1 ws)
    (assembled asm1)
)

(:goal (and 
  (assembled asm6) 
))
)
