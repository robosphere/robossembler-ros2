(define (problem box_problem)
(:domain atomic_domain)
(:objects
    ur_manipulator_gripper - arm 
    workspace1 - zone
    box1 box2 box3 - part
    asm0 asm1 asm2 asm3 - assembly
)

(:init 
    (part_of box1 asm1) 
    (part_of box2 asm2)
    (part_of box3 asm3)
    (assembly_order asm0 asm1)
    (assembly_order asm1 asm2)
    (assembly_order asm2 asm3)

    (arm_available ur_manipulator_gripper)
    (assembly_at asm0 workspace1)
    (assembled asm0)
)

(:goal (and 
  (assembled asm3) 
))
)
